import http from "@/utils/request"

export default {
	list: {
		url: '/role/index',
		name: "获取角色列表",
		get: async function(data={}) {
			return await http.get(this.url, data);
		}
	},
	add: {
		url: '/role/add',
		name: "添加角色",
		post: async function(data={}) {
			return await http.post(this.url, data);
		}
	},
	edit: {
		url: '/role/edit',
		name: "编辑觉得",
		post: async function(data={}) {
			return await http.post(this.url, data);
		}
	},
	del: {
		url: '/role/del',
		name: "删除角色",
		get: async function(data={}) {
			return await http.get(this.url, data);
		}
	},
	getNode: {
		url: '/role/showNode',
		name: "获取节点树",
		get: async function(){
			return await http.get(this.url);
		}
	},
}