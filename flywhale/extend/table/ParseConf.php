<?php

namespace table;

use think\facade\Db;

class ParseConf
{
    /**
     * 增量更新主表
     * @param $param
     */
    public static function incTable($param)
    {
        list($newColumnMap, $tableName, $column2title, $column2type) = $param;

        $oldColumnsList = Db::query('SHOW COLUMNS FROM `' . $tableName . '`');
        $oldColumnsMap = [];
        foreach ($oldColumnsList as $vo) {
            if (in_array($vo['Field'], ['id', 'create_time', 'update_time', 'delete_time'])) {
                continue;
            }

            $oldColumnsMap[] = $vo['Field'];
        }

        $dropColumns = array_diff($oldColumnsMap, $newColumnMap);
        $addColumns = array_diff($newColumnMap, $oldColumnsMap);

        // 先删除旧的字段
        if (!empty($dropColumns)) {
            foreach ($dropColumns as $vo) {
                Db::execute('alter table `' . $tableName . '` drop column `' . $vo . '`;');
            }
        }

        if (!empty($addColumns)) {
            foreach ($addColumns as $vo) {
                if ($column2type[$vo] == 'ueditor') {
                    Db::execute('alter table `' . $tableName . '` add column `' . $vo . '` longtext NULL COMMENT "' . $column2title[$vo] . '";');
                } else {
                    Db::execute('alter table `' . $tableName . '` add column `' . $vo . '` varchar(255) NULL COMMENT "' . $column2title[$vo] . '";');
                }
            }
        }
    }

    /**
     * 覆盖生成主表
     * @param $param
     */
    public static function coverTable($param)
    {
        list($newColumnMap, $tableName, $column2title, $column2type) = $param;

        Db::query('DROP TABLE IF EXISTS `' . $tableName . '`');
        self::makeNewTable($param);
    }

    /**
     * 生成新主表
     * @param $param
     */
    public static function makeNewTable($param)
    {
        list($newColumnMap, $tableName, $column2title, $column2type, $title) = $param;

        $column = '';
        // 表压根都不存在，就直接处理
        foreach ($newColumnMap as $key => $vo) {
            // 强迫症为了dd打印的时候格式对其，其实没啥意义
            $tab = '';
            if ($key > 0) {
                $tab = '    ';
            }

            if ($column2type[$vo] == 'ueditor') {
                $column .= $tab . '`' . $vo . '` longtext NULL DEFAULT NULL COMMENT "' . $column2title[$vo] . '",' . PHP_EOL;
            } else {
                $column .= $tab . '`' . $vo . '` varchar(255) NULL DEFAULT NULL COMMENT "' . $column2title[$vo] . '",' . PHP_EOL;
            }
        }
            $sql = <<<EOL
CREATE TABLE `{$tableName}` (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT "id",
    {$column}    `create_time` datetime NULL DEFAULT NULL COMMENT "创建时间",
    `update_time` datetime NULL DEFAULT NULL COMMENT "更新时间",
    `delete_time` datetime NULL DEFAULT NULL COMMENT "删除时间",
     PRIMARY KEY (`id`) 
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 COMMENT='{$title}';
EOL;
            Db::execute($sql);
    }

    /**
     * 增量更新子表
     * @param $param
     */
    public static function incSubTable($param)
    {
        list($newColumnMap, $tableName, $column2title, $fieldId) = $param;

        $oldColumnsList = Db::query('SHOW COLUMNS FROM `' . $tableName . '`');
        $oldColumnsMap = [];
        foreach ($oldColumnsList as $vo) {
            if (in_array($vo['Field'], ['id', $fieldId, 'create_time', 'update_time', 'delete_time'])) {
                continue;
            }

            $oldColumnsMap[] = $vo['Field'];
        }

        $dropColumns = array_diff($oldColumnsMap, $newColumnMap);
        $addColumns = array_diff($newColumnMap, $oldColumnsMap);

        // 先删除旧的字段
        if (!empty($dropColumns)) {
            foreach ($dropColumns as $vo) {
                Db::execute('alter table `' . $tableName . '` drop column `' . $vo . '`;');
            }
        }

        if (!empty($addColumns)) {
            foreach ($addColumns as $vo) {
                Db::execute('alter table `' . $tableName . '` add column `' . $vo . '` varchar(255) NULL COMMENT "' . $column2title[$vo] . '";');
            }
        }
    }

    /**
     * 覆盖生成子表
     * @param $param
     */
    public static function coverSubTable($param)
    {
        list($newColumnMap, $tableName, $column2title, $title, $fieldId) = $param;

        Db::query('DROP TABLE IF EXISTS `' . $tableName . '`');
        self::makeNewSubTable($param);
    }

    /**
     * 生成新子表
     * @param $param
     */
    public static function makeNewSubTable($param)
    {
        list($newColumnMap, $tableName, $column2title, $title, $fieldId) = $param;

        $column = '`' . $fieldId . '` int(11) UNSIGNED NOT NULL COMMENT "关联键",' . PHP_EOL;
        // 表压根都不存在，就直接处理
        foreach ($newColumnMap as $key => $vo) {
            // 强迫症为了dd打印的时候格式对其，其实没啥意义
            $tab = ' ';
            if ($key >= 0) {
                $tab = '    ';
            }

            $column .= $tab . '`' . $vo . '` varchar(255) NULL DEFAULT NULL COMMENT "' . $column2title[$vo] . '",' . PHP_EOL;
        }
        $sql = <<<EOL
CREATE TABLE `{$tableName}` (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT "id",
    {$column}    `create_time` datetime NULL DEFAULT NULL COMMENT "创建时间",
    `update_time` datetime NULL DEFAULT NULL COMMENT "更新时间",
    `delete_time` datetime NULL DEFAULT NULL COMMENT "删除时间",
     PRIMARY KEY (`id`) 
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 COMMENT='{$title}';
EOL;
        Db::execute($sql);
    }
}