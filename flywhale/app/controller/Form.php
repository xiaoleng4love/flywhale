<?php

namespace app\controller;

use app\service\FormService;
use think\annotation\Inject;

class Form extends Base
{
    /**
     * @Inject()
     * @var FormService
     */
    protected $formService;

    public function index()
    {
        $param = input('param.');

        $res = $this->formService->getFormList($param);
        return json(pageReturn($res));
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->formService->addForm($param);
            return json($res);
        }
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            if (isset($param['form_json']) && !empty($param['form_json'])) {
                $param['form_json'] = json_encode($param['form_json']);
            }

            $res = $this->formService->editForm($param);
            return json($res);
        }
    }

    public function deploy()
    {
        $param = input('post.');

        $res = $this->formService->deploy($param);
        return json($res);
    }

    public function undeploy()
    {
        $id = input('param.id');
        $type = input('param.type');

        $res = $this->formService->undeploy($id, $type);
        return json($res);
    }

    public function allForm()
    {
        return json($this->formService->getAllForm());
    }

    public function del()
    {
        return json($this->formService->delForm(input('param.id')));
    }
}