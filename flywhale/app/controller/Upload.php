<?php

namespace app\controller;

class Upload extends Base
{
    //上传图片
    public function doUpload()
    {
        $file = request()->file('file');

        // 移动到框架应用根目录/public/uploads/ 目录下
        $saveName = \think\facade\Filesystem::disk('public')->putFile('', $file);

        return jsonReturn(0, '上传成功', [
            'src' => request()->domain() . '/storage/' . $saveName,
            'name' => $file->getOriginalName()
        ]);
    }
}
