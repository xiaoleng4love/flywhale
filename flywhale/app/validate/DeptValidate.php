<?php

namespace app\validate;

use think\Validate;

class DeptValidate extends Validate
{
    protected $rule = [
        'name|部门名称' => 'require|max:55',
        'pid|上级部门' => 'require',
        'status|状态' => 'require'
    ];
}