<?php

namespace app\model;

use think\facade\Log;

class User extends BaseModel
{
    /**
     * 与部门表关联
     * @return \think\model\relation\HasOne
     */
    public function dept()
    {
        return $this->hasOne(Dept::class, 'id', 'dept_id');
    }

    /**
     * 与角色表关联
     * @return \think\model\relation\HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
     * 根据名称获取管理员信息
     * @param $name
     * @return array
     */
    public function getInfoByName($name)
    {
        try {

            $info = $this->where('name', $name)->find();
        } catch (\Exception $e) {
            Log::error('获取管理员信息错误:' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $info);
    }

    /**
     * 根据id更新管理员信息
     * @param $param
     * @param $id
     * @return array
     */
    public function updateInfoById($param, $id)
    {
        try {

            $this->where('id', $id)->update($param);
        } catch (\Exception $e) {
            Log::error('更新管理员信息错误:' .  $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success');
    }

    /**
     * 根据角色获取用户信息
     * @param $roleId
     * @return array
     */
    public function getUserByRoleId($roleId)
    {
        try {

            $list = $this->where('role_id', $roleId)->select();
        } catch (\Exception $e) {
            Log::error('根据角色获取用户信息错误:' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 根据部门id获取用户
     * @param $deptId
     * @return array
     */
    public function getUserByDeptId($deptId)
    {
        try {

            $list = $this->where('dept_id', $deptId)->select();
        } catch (\Exception $e) {
            Log::error('根据部门获取用户信息错误:' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 获取用户列表
     * @param $limit
     * @param $where
     * @return array
     */
    public function getUserList($limit, $where)
    {
        try {

            $list = $this->with(['dept', 'role'])->where($where)->where('id', '>', 1)->order('id', 'desc')->paginate($limit);
        }catch (\Exception $e) {
            Log::error('获取用户列表错误: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 添加用户
     * @param $param
     * @return array
     */
    public function addUser($param)
    {
        try {

            $has = $this->where('name', $param['name'])->find();
            if (!empty($has)) {
                return dataReturn(-3, '该员工已经存在，不可重复添加');
            }

            $param['create_time'] = date('Y-m-d H:i:s');
            $userId = $this->insertGetId($param);
        }catch (\Exception $e) {
            Log::error('添加用户错误: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-2, $e->getMessage());
        }

        return dataReturn(0, 'success', ['id' => $userId]);
    }

    /**
     * 编辑用户
     * @param $param
     * @return array
     */
    public function editUser($param)
    {
        try {

            $has = $this->where('name', $param['name'])->where('id', '<>', $param['id'])->find();
            if (!empty($has)) {
                return dataReturn(-3, '该员工已经存在，不可重复添加');
            }

            $param['update_time'] = date('Y-m-d H:i:s');
            $this->where('id', $param['id'])->update($param);
        }catch (\Exception $e) {
            Log::error('编辑用户错误: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-2, $e->getMessage());
        }

        return dataReturn(0, 'success');
    }

    /**
     * 删除用户
     * @param $userId
     * @return array
     */
    public function delUser($userId)
    {
        try {

            $this->where('id', $userId)->delete();
        }catch (\Exception $e) {
            Log::error('删除用户错误: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-2, $e->getMessage());
        }

        return dataReturn(0, 'success');
    }

    /**
     * 根据id获取用户信息
     * @param $userId
     * @return array
     */
    public function getUserById($userId)
    {
        try {

            $info = $this->where('id', $userId)->find();
        }catch (\Exception $e) {
            Log::error('根据id获取客户信息错误: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-2, $e->getMessage());
        }

        return dataReturn(0, 'success', $info);
    }
}