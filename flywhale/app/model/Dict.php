<?php

namespace app\model;

use think\facade\Log;

class Dict extends BaseModel
{
    /**
     * 获取分类列表
     * @param $id
     * @return array
     */
    public function getDictById($id)
    {
        try {

            $info = $this->where('id', $id)->find();
        } catch (\Exception $e) {
            Log::error('根据ID获取分类错误: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $info);
    }

    /**
     * 根据分类获取字典列表
     * @param $id
     * @return array
     */
    public function getDictByCateId($id)
    {
        try {

            $list = $this->where('cate_id', $id)->select()->toArray();
        } catch (\Exception $e) {
            Log::error('根据分类获取字典列表错误: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 获取分类列表
     * @param $limit
     * @param $where
     * @return array
     */
    public function getDictList($limit, $where)
    {
        try {

            $list = $this->where($where)->order('id', 'desc')->paginate($limit);
        }catch (\Exception $e) {
            Log::error('获取分类列表错误: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 添加字典
     * @param $param
     * @return array
     */
    public function addDict($param)
    {
        try {

            $info = $this->where('key', $param['key'])->where('cate_id', $param['cate_id'])->find();
            if (!empty($info)) {
                return dataReturn(-2, '该字典已经存在');
            }

            $this->insert($param);
        }catch (\Exception $e) {
            Log::error('添加字典失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '添加成功');
    }

    /**
     * 编辑字典
     * @param $param
     * @return array
     */
    public function editDict($param)
    {
        try {

            $info = $this->where('key', $param['key'])->where('cate_id', $param['cate_id'])
                ->where('id', '<>', $param['id'])->find();
            if (!empty($info)) {
                return dataReturn(-2, '该字典已经存在');
            }

            $this->where('id', $param['id'])->update($param);
        }catch (\Exception $e) {
            Log::error('编辑字典失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '编辑成功');
    }

    /**
     * 删除字典
     * @param $id
     * @return array
     */
    public function delDict($id)
    {
        try {

            $this->where('id', $id)->delete();
        }catch (\Exception $e) {
            Log::error('删除字典失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '删除成功');
    }
}