<?php

namespace app\service;

use app\model\DeptLeader;
use app\model\User;
use app\validate\UserValidate;
use think\annotation\Inject;
use think\exception\ValidateException;

class UserService
{
    /**
     * @Inject()
     * @var User
     */
    protected $userModel;

    /**
     * @Inject()
     * @var DeptLeader
     */
    protected $deptLeaderModel;

    /**
     * 获取用户列表
     * @param $param
     * @return array
     */
    public function getUserList($param)
    {
        $limit = $param['pageSize'];

        $where = [];
        if (!empty($param['nickname'])) {
            $where[] = ['nickname', 'like', '%' . $param['nickname'] . '%'];
        }

        if (!empty($param['dept_id'])) {
            $where[] = ['dept_id', '=', $param['dept_id']];
        }

        return $this->userModel->getUserList($limit, $where);
    }

    /**
     * 添加用户
     * @param $param
     * @return array
     */
    public function addUser($param)
    {
        try {

            validate(UserValidate::class)->check($param);
        } catch (ValidateException $e) {
            return dataReturn(-1, $e->getError());
        }

        $param['salt'] = uniqid();
        $param['password'] = makePassword($param['password'], $param['salt']);

        $res = $this->userModel->addUser($param);
        if ($res['code'] != 0) {
            return $res;
        }

        // 若当前用户是主管
        if ($param['is_leader'] == 1) {
            $this->deptLeaderModel->addLeader([
                'user_id' => $res['data']['id'],
                'dept_id' => $param['dept_id']
            ]);
        }

        return dataReturn(0, '添加成功');
    }

    /**
     * 编辑用户
     * @param $param
     * @return array
     */
    public function editUser($param)
    {
        try {

            validate(UserValidate::class)->scene('edit')->check($param);
        } catch (ValidateException $e) {
            return dataReturn(-1, $e->getError());
        }

        if (!empty($param['password'])) {
            $param['salt'] = uniqid();
            $param['password'] = makePassword($param['password'], $param['salt']);
        } else {
            unset($param['password']);
        }

        $res = $this->userModel->editUser($param);
        if ($res['code'] != 0) {
            return $res;
        }

        $this->deptLeaderModel->checkAndChange([
            'is_leader' => $param['is_leader'] ,
            'user_id' => $param['id'],
            'dept_id' => $param['dept_id']
        ]);

        return dataReturn(0, '编辑成功');
    }

    /**
     * 删除用户
     * @param $userId
     * @return array
     */
    public function delUser($userId)
    {
        if ($userId == 1) {
            return dataReturn(-2, '超级管理员不可删除');
        }

        $res = $this->userModel->delUser($userId);
        if ($res['code'] != 0) {
            return $res;
        }

        $this->deptLeaderModel->checkAndDelete($userId);
        return dataReturn(0, '删除成功');
    }

    /**
     * 获取所有的用户
     * @return array
     */
    public function getAllUser()
    {
        return $this->userModel->getAllList(['status' => 1], 'id value,nickname label,dept_id');
    }
}